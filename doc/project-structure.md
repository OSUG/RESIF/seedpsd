# SeedPSD documentation

[Back to index](index.md)

## Project structure

### /seedpsd

Main folder containing the SeedPSD application.

#### /seedpsd/alembic

Database migration system.

##### /seedpsd/alembic/versions

Database structure migrations.

##### /seedpsd/alembic/env.py

Alembic configuration.

##### /seedpsd/alembic/script.py.mako

Template of database migration file to generate.
 
#### /seedpsd/api

Frontend part of SeedPSD built on the [Pyramid](https://trypyramid.com/) framework.

##### /seedpsd/api/panels

Controller part of configurable blocks (from settings) within templates.

##### /seedpsd/api/skins

Templates and static files used by the template overloading system.

##### /seedpsd/api/static

Static files used within the templates.

##### /seedpsd/api/templates

HTML templates using the [Jinja2](https://jinja.palletsprojects.com/) engine.

###### /seedpsd/api/templates/panels

HTML templates related to panels.

###### /seedpsd/api/templates/snippets

Reusable fragments of HTML templates.

###### /seedpsd/api/templates/views

HTML templates related to views.

###### /seedpsd/api/templates/layout.jinja2

The main HTML layout.

##### /seedpsd/api/layout.py

The main layout controller.

##### /seedpsd/api/__init__.py

THe main entry point of the API system.

#### /seedpsd/cli

Command-line part of SeedPSD built on the [Click](https://click.palletsprojects.com/) framework.

##### /seedpsd/cli/commands

Sub-commands for CLI.

#### /seedpsd/locale

Directories storing localization strings for each configured language.

#### /seedpsd/models

Application data structures

##### /seedpsd/models/db

Database mapping classes

##### /seedpsd/models/files

MSEED (and NPZ) file and path handling system.

#### /seedpsd/settings

##### /seedpsd/settings/plots

Settings related to plotting system.

##### /seedpsd/settings/alembic.py

Settings related to Alembic.

##### /seedpsd/settings/workers.py

Main settings related to the whole application.

#### /seedpsd/tools

Collection of several common tools.

#### /seedpsd/workers

Main parts of the application.

##### /seedpsd/workers/data

Workflow tasks related to data processing.

##### /seedpsd/workers/amqp

AMQP event listeners.

##### /seedpsd/workers/metadata.py

Workflow tasks related to metadata processing.

##### /seedpsd/workers/plot.py

Workflow tasks related to plot rendering.

##### /seedpsd/workers/ppsd.py

Workflow tasks related to PSD calculation.

##### /seedpsd/workers/value.py

Workflow tasks related to numerical value extraction.

### /tests

Unit tests built on [pytest](https://docs.pytest.org/).

### /.env-skeleton

Skeleton of configuration file.

### /requirements.txt

Main dependencies.

### /requirements-develop.txt

Additional dependencies for development.

### /setup.py

Project setup system.

