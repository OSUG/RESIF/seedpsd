# SeedPSD documentation

## Setup
* [First install](install.adoc)
* [Upgrade](upgrade.adoc)
* [Configuration](config.adoc)

## Usage
* [First steps](first-steps.adoc)
* [Command-line](cli-usage.adoc)

## Optional features
* [PPSD customization](ppsd.md)
* [AMQP event listeners](server-amqp.adoc)
* [Web Server](server-web.adoc)

## Technical details

* [Known limitations](known-limitations.md)
* [Database](database.adoc)
* [Process workflows](workflows.adoc)
* [Project structure](project-structure.md)
* [Software dependencies](dependencies.md)
