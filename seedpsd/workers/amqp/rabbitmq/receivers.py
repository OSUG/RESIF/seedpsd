import pika
import verboselogs

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdAmqpReceiverRabbitmq:
    """
    Base AMQP message handler with "RabbitMQ" backend
    """

    def __init__(self, processor, settings, simulate=False):
        self._processor = processor
        self._settings = settings
        self._simulate = simulate

        self._exchange_name = None
        self._exchange_type = None
        self._exchange_durable = None
        self._queue_name = None
        self._queue_routing = None
        self._queue_durable = None

    def receive(self):
        """
        Connect to RabbitMQ
        :return:
        """
        logger.debug("SeedPsdAmqpReceiverRabbitmqData.receive()")

        # Start connection
        logger.info("Connecting AMQP receiver to %s", self._settings.amqp_server_url)
        credentials = pika.PlainCredentials(
            self._settings.amqp_user, self._settings.amqp_password
        )
        parameters = pika.ConnectionParameters(
            self._settings.amqp_server_url,
            self._settings.amqp_port,
            self._settings.amqp_vhost,
            credentials,
            heartbeat=self._settings.amqp_heartbeat,
            blocked_connection_timeout=self._settings.amqp_timeout,
        )
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.basic_qos(prefetch_count=1)

        # Initialize exchange
        logger.verbose("Declaring the '%s' exchange", self._exchange_name)
        channel.exchange_declare(
            exchange=self._exchange_name,
            exchange_type=self._exchange_type,
            durable=self._exchange_durable,
        )

        # Initialize queue
        logger.verbose("Declaring the '%s' queue", self._queue_name)
        channel.queue_declare(queue=self._queue_name, durable=self._queue_durable)

        # Bind queue to exchange
        logger.verbose(
            "Binding the '%s' queue to the '%s' exchange using '%s' routing key",
            self._queue_name,
            self._exchange_name,
            self._queue_routing,
        )
        channel.queue_bind(
            queue=self._queue_name,
            exchange=self._exchange_name,
            routing_key=self._queue_routing,
        )

        # Start queue
        logger.info("Start consuming the '%s' queue", self._queue_name)
        channel.basic_consume(
            queue=self._queue_name, on_message_callback=self._on_message, auto_ack=False
        )
        channel.start_consuming()

    def _on_message(self, channel, method, properties, body):
        logger.debug(
            "SeedPsdAmqpReceiverRabbitmq.on_message(%s, %s, %s, %s)",
            channel,
            method,
            properties,
            body,
        )

        # Process the message
        self._processor.process(body)
        channel.basic_ack(delivery_tag=method.delivery_tag)


class SeedPsdAmqpReceiverRabbitmqData(SeedPsdAmqpReceiverRabbitmq):
    def __init__(self, processor, settings, simulate=False):
        super().__init__(processor, settings, simulate)
        self._exchange_name = self._settings.amqp_exchange_name
        self._exchange_type = self._settings.amqp_exchange_type
        self._exchange_durable = self._settings.amqp_exchange_durable
        self._queue_name = self._settings.amqp_data_queue_name
        self._queue_routing = self._settings.amqp_data_queue_routing
        self._queue_durable = self._settings.amqp_data_queue_durable


class SeedPsdAmqpReceiverRabbitmqMetadata(SeedPsdAmqpReceiverRabbitmq):
    def __init__(self, processor, settings, simulate=False):
        super().__init__(processor, settings, simulate)
        self._exchange_name = self._settings.amqp_exchange_name
        self._exchange_type = self._settings.amqp_exchange_type
        self._exchange_durable = self._settings.amqp_exchange_durable
        self._queue_name = self._settings.amqp_metadata_queue_name
        self._queue_routing = self._settings.amqp_metadata_queue_routing
        self._queue_durable = self._settings.amqp_metadata_queue_durable
