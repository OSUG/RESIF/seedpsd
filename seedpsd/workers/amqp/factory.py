from .rabbitmq.workers import SeedPsdWorkerRabbitmq


class SeedPsdWorkerAmqpFactory:
    WORKER_DATA = "data"
    WORKER_METADATA = "metadata"

    @classmethod
    def factory(cls, worker_type, settings):
        return SeedPsdWorkerRabbitmq.factory(worker_type, settings)

    @classmethod
    def factory_data(cls, settings):
        return cls.factory(cls.WORKER_DATA, settings)

    @classmethod
    def factory_metadata(cls, settings):
        return cls.factory(cls.WORKER_METADATA, settings)
