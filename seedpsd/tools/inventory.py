import os.path

import verboselogs
from obspy.core.inventory.inventory import read_inventory

logger = verboselogs.VerboseLogger(__name__)


class InventoryFactory:
    @staticmethod
    def from_webservice(
        fdsn_client,
        level="channel",
        network=None,
        station=None,
        location=None,
        channel=None,
        start_before=None,
        end_after=None,
    ):
        """
        Get an inventory from a FDSN webservice

        :param fdsn_client: The initialized FDSN client
        :param level: (Optional) Level (network|station|channel|response) - Default:channel
        :param network: (Optional) Network code
        :param station: (Optional) Station code
        :param location: (Optional) Location code
        :param channel: (Optional) Channel code
        :param start_before: (Optional) Select results staring before this date
        :param end_after: (Optional) Select results ending after this date
        :return: An inventory
        """
        logger.debug(
            f"InventoryFactory.from_webservice({fdsn_client}, {level}, {network}, {station}, {location}, {channel}, {start_before}, {end_after})"
        )

        params = {"includerestricted": True}

        if network is not None:
            params["network"] = network
        if station is not None:
            params["station"] = station
        if location is not None:
            params["location"] = location
        if channel is not None:
            channel = channel.split(".")[0]
            params["channel"] = channel
        if level is not None:
            params["level"] = level
        if start_before is not None:
            params["start_before"] = start_before
        if end_after is not None:
            params["end_after"] = end_after

        return fdsn_client.get_stations(**params)

    @staticmethod
    def from_filesystem(metadata_path, level="channel", network=None, station=None):
        """
        Get an inventory from the filesystem

        :param metadata_path: The path containing the metadata files
        :param level: (Optional) Level (network|station|channel|response) - Default:channel
        :param network: (Optional) Network code
        :param station: (Optional) Station code
        :return: An inventory
        """
        logger.debug(
            f"InventoryFactory.from_filesystem({metadata_path}, {level}, {network}, {station})"
        )

        if os.path.isdir(metadata_path):
            metadata_path = os.path.join(
                metadata_path, f"{network or '*'}.{station or '*'}.xml"
            )

        logger.verbose(f"Loading metadata from {metadata_path}")
        return read_inventory(metadata_path, format="STATIONXML", level=level)
