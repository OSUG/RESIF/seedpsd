"""Multi-tenancy using a dedicated schema by network, and 1 shared schema

Revision ID: 385990f03d31
Revises: f64698b802b8
Create Date: 2023-10-24 17:09:09.188764

"""

import logging

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql
from sqlalchemy.schema import CreateSchema, DropSchema
from sqlalchemy_utils.types.json import JSONType
from sqlalchemy_utils.types.uuid import UUIDType

from seedpsd.tools.common import is_temporary_network

# revision identifiers, used by Alembic.
revision = "385990f03d31"
down_revision = "f64698b802b8"
branch_labels = None
depends_on = None


# UPGRADE ##############################################################################################################


def upgrade():
    # Get connection
    connection = op.get_bind()

    # Migrate shared schema
    upgrade_shared(connection)

    # Migrate each network
    for network in connection.execute(sa.text("SELECT id, schema FROM shared.network")):
        upgrade_tenant(connection, network.schema, network.id)

    # # Drop old schema
    connection.execute(DropSchema("psd", if_exists=True, cascade=True))
    # ### end Alembic commands ###


def create_schema_shared(connection):
    connection.execute(CreateSchema("shared", if_not_exists=True))

    # Network
    table_network = op.create_table(
        "network",
        sa.Column("id", UUIDType(), nullable=False),
        sa.Column("code", sa.String(), nullable=False),
        sa.Column("year", sa.Integer(), nullable=False),
        sa.Column("start", sa.Date(), nullable=False),
        sa.Column("end", sa.Date(), nullable=True),
        sa.Column("temporary", sa.Boolean(), nullable=False),
        sa.Column("schema", sa.String(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_network")),
        sa.UniqueConstraint("schema", name=op.f("uq_network_schema")),
        schema="shared",
    )
    op.create_index(
        "ix_network_code", "network", ["code"], unique=False, schema="shared"
    )
    op.create_index(
        "ix_network_start", "network", ["start"], unique=False, schema="shared"
    )
    op.create_index(
        "ix_network_year", "network", ["year"], unique=False, schema="shared"
    )

    # Source
    table_source = op.create_table(
        "source",
        sa.Column("id", UUIDType(), nullable=False),
        sa.Column("network_id", UUIDType(), nullable=False),
        sa.Column("station", sa.String(), nullable=False),
        sa.Column("location", sa.String(), nullable=True),
        sa.Column("channel", sa.String(), nullable=False),
        sa.ForeignKeyConstraint(
            ["network_id"],
            ["shared.network.id"],
            name=op.f("fk_source_network_id_network"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_source")),
        schema="shared",
    )
    op.create_index(
        "ix_source_channel", "source", ["channel"], unique=False, schema="shared"
    )
    op.create_index(
        "ix_source_location", "source", ["location"], unique=False, schema="shared"
    )
    op.create_index(
        "ix_source_network", "source", ["network_id"], unique=False, schema="shared"
    )
    op.create_index(
        "ix_source_station", "source", ["station"], unique=False, schema="shared"
    )
    op.create_unique_constraint(
        op.f("uq_source_network_id"),
        "source",
        ["network_id", "station", "location", "channel"],
        schema="shared",
    )

    # File
    op.create_table(
        "file",
        sa.Column("id", UUIDType(), nullable=False),
        sa.Column("source_id", UUIDType(), nullable=True),
        sa.Column("date", sa.Date(), nullable=False),
        sa.Column("year", sa.Integer(), nullable=False),
        sa.Column("day", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.Column("processed", sa.Boolean(), nullable=False),
        sa.Column("metadata", sa.Boolean(), nullable=False),
        sa.Column("date_created", sa.DateTime(), nullable=True),
        sa.Column("date_processed", sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(
            ["source_id"],
            ["shared.source.id"],
            name=op.f("fk_file_source_id_source"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_file")),
        sa.UniqueConstraint("name", name=op.f("uq_file_name")),
        schema="shared",
    )
    op.create_index("ix_file_date", "file", ["date"], unique=False, schema="shared")
    op.create_index(
        "ix_file_source", "file", ["source_id"], unique=False, schema="shared"
    )

    return table_network, table_source


def create_schema_tenant(connection, schema):
    connection.execute(CreateSchema(schema, if_not_exists=True))

    # Epoch
    op.create_table(
        "epoch",
        sa.Column("id", UUIDType(), nullable=False),
        sa.Column("source_id", UUIDType(), nullable=False),
        sa.Column("start", sa.DateTime(), nullable=False),
        sa.Column("end", sa.DateTime(), nullable=True),
        sa.Column("instrument", JSONType(), nullable=True),
        sa.ForeignKeyConstraint(
            ["source_id"],
            ["shared.source.id"],
            name=op.f("fk_epoch_source_id_source"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_epoch")),
        schema=schema,
    )
    op.create_index(
        "ix_epoch_source", "epoch", ["source_id"], unique=False, schema=schema
    )

    # Stream
    op.create_table(
        "stream",
        sa.Column("id", UUIDType(), nullable=False),
        sa.Column("file_id", UUIDType(), nullable=False),
        sa.Column("source_id", UUIDType(), nullable=False),
        sa.Column("epoch_id", UUIDType(), nullable=True),
        sa.Column(
            "valid", sa.Boolean(), server_default=sa.text("true"), nullable=False
        ),
        sa.Column("start", sa.DateTime(), nullable=False),
        sa.Column("end", sa.DateTime(), nullable=False),
        sa.Column("sampling_rate", sa.Float(), nullable=False),
        sa.ForeignKeyConstraint(
            ["epoch_id"],
            [f"{schema}.epoch.id"],
            name=op.f("fk_stream_epoch_id_epoch"),
            onupdate="CASCADE",
            ondelete="SET NULL",
        ),
        sa.ForeignKeyConstraint(
            ["file_id"],
            ["shared.file.id"],
            name=op.f("fk_stream_file_id_file"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["source_id"],
            ["shared.source.id"],
            name=op.f("fk_stream_source_id_source"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_stream")),
        schema=schema,
    )
    op.create_index(
        "ix_stream_epoch", "stream", ["epoch_id"], unique=False, schema=schema
    )
    op.create_index(
        "ix_stream_file", "stream", ["file_id"], unique=False, schema=schema
    )
    op.create_index(
        "ix_stream_source", "stream", ["source_id"], unique=False, schema=schema
    )

    # Trace
    op.create_table(
        "trace",
        sa.Column("stream_id", UUIDType(), nullable=False),
        sa.Column("timestamp", sa.DateTime(), nullable=False),
        sa.Column("data", postgresql.ARRAY(sa.Float()), nullable=True),
        sa.ForeignKeyConstraint(
            ["stream_id"],
            [f"{schema}.stream.id"],
            name=op.f("fk_trace_stream_id_stream"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("stream_id", "timestamp", name=op.f("pk_trace")),
        schema=schema,
    )
    op.create_index(
        "ix_trace_stream", "trace", ["stream_id"], unique=False, schema=schema
    )

    # Gap
    op.create_table(
        "gap",
        sa.Column("stream_id", UUIDType(), nullable=False),
        sa.Column("start", sa.DateTime(), nullable=False),
        sa.Column("end", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(
            ["stream_id"],
            [f"{schema}.stream.id"],
            name=op.f("fk_gap_stream_id_stream"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("stream_id", "start", name=op.f("pk_gap")),
        schema=schema,
    )
    op.create_index("ix_gap_stream", "gap", ["stream_id"], unique=False, schema=schema)

    # Overlap
    op.create_table(
        "overlap",
        sa.Column("stream_id", UUIDType(), nullable=False),
        sa.Column("start", sa.DateTime(), nullable=False),
        sa.Column("end", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(
            ["stream_id"],
            [f"{schema}.stream.id"],
            name=op.f("fk_overlap_stream_id_stream"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        # sa.PrimaryKeyConstraint('stream_id', 'start', name=op.f('pk_overlap')),
        schema=schema,
    )
    op.create_index(
        "ix_overlap_stream", "overlap", ["stream_id"], unique=False, schema=schema
    )


def upgrade_shared(connection):
    logging.info("Migrating schema: shared")

    # Create shared schema
    shared_network, shared_source = create_schema_shared(connection)

    # Migrate networks
    items = {}
    duplicated = {}
    for network in connection.execute(sa.text('select * from "psd"."network"')):
        # Compute tenant schema name
        if is_temporary_network(network.code):
            schema = f"network_{network.code}{network.year}"
        else:
            schema = f"network_{network.code}"

        # Migrate networks without duplication
        if schema in items:
            if schema not in duplicated:
                duplicated[schema] = []
            duplicated[schema].append(network.id)

        else:
            items[schema] = {
                "id": network.id,
                "code": network.code,
                "year": network.year,
                "start": network.start,
                "end": network.end,
                "temporary": network.temporary,
                "schema": schema,
            }
    op.bulk_insert(shared_network, list(items.values()))

    # Migrate sources
    for schema in items:
        connection.execute(
            sa.text(
                'INSERT INTO "shared"."source" (SELECT * FROM "psd"."source" WHERE source.network_id = :network_id)'
            ),
            {"network_id": items[schema]["id"]},
        )

        if schema in duplicated:
            sources = []
            for duplicated_network_id in duplicated[schema]:
                for source in connection.execute(
                    sa.text(
                        'SELECT * FROM "psd"."source" WHERE source.network_id = :network_id'
                    ),
                    {"network_id": duplicated_network_id},
                ):
                    sources.append(
                        {
                            "id": source.id,
                            "network_id": items[schema]["id"],
                            "station": source.station,
                            "location": source.location,
                            "channel": source.channel,
                        }
                    )
            op.bulk_insert(shared_source, sources)

    # Migrate files
    connection.execute(
        sa.text('INSERT INTO "shared"."file" (SELECT * FROM "psd"."file")')
    )


def upgrade_tenant(connection, schema, network_id):
    logging.info(f"Migrating schema: {schema}")

    # Create schema
    create_schema_tenant(connection, schema)

    # Migrate epochs
    connection.execute(
        sa.text(
            f'INSERT INTO "{schema}"."epoch" (SELECT epoch.* FROM "psd"."epoch" JOIN "psd"."source" ON epoch.source_id = source.id WHERE source.network_id = :network_id)'
        ),
        {"network_id": network_id},
    )

    # Migrate streams
    connection.execute(
        sa.text(
            f'INSERT INTO "{schema}"."stream" (SELECT stream.* FROM "psd"."stream" JOIN "psd"."source" ON stream.source_id = source.id WHERE source.network_id = :network_id)'
        ),
        {"network_id": network_id},
    )

    # Migrate traces
    connection.execute(
        sa.text(
            f'INSERT INTO "{schema}"."trace" (SELECT trace.* FROM "psd"."trace" JOIN "psd"."stream" ON trace.stream_id = stream.id JOIN "psd"."source" ON stream.source_id = source.id WHERE source.network_id = :network_id)'
        ),
        {"network_id": network_id},
    )

    # Migrate gaps / overlaps
    connection.execute(
        sa.text(
            f'INSERT INTO "{schema}"."gap" (SELECT DISTINCT gap.* FROM "psd"."gap" JOIN "psd"."stream" ON gap.stream_id = stream.id JOIN "psd"."source" ON stream.source_id = source.id WHERE source.network_id = :network_id AND gap.start < gap.end)'
        ),
        {"network_id": network_id},
    )
    connection.execute(
        sa.text(
            f'INSERT INTO "{schema}"."overlap" (SELECT DISTINCT gap.* FROM "psd"."gap" JOIN "psd"."stream" ON gap.stream_id = stream.id JOIN "psd"."source" ON stream.source_id = source.id WHERE source.network_id = :network_id AND gap.start >= gap.end)'
        ),
        {"network_id": network_id},
    )

    # Delete old data related
    # connection.execute(sa.text(f'DELETE from "psd"."source" where source.network_id = :network_id'), {'network_id': network_id})


# DOWNGRADE ############################################################################################################


def downgrade():
    # Get connection
    connection = op.get_bind()

    # Migrate shared data
    downgrade_shared(connection)

    # Migrate each network
    for network in connection.execute(sa.text("SELECT id, schema FROM shared.network")):
        downgrade_tenant(connection, network.schema, network.id)
        connection.execute(DropSchema(network.schema, if_exists=True, cascade=True))

    # Drop old schema
    connection.execute(DropSchema("shared", if_exists=True, cascade=True))
    # ### end Alembic commands ###


def create_schema_psd(connection):
    connection.execute(CreateSchema("psd", if_not_exists=True))

    # Network
    table_network = op.create_table(
        "network",
        sa.Column("id", sa.UUID(), autoincrement=False, nullable=False),
        sa.Column("code", sa.VARCHAR(), autoincrement=False, nullable=False),
        sa.Column("year", sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column("start", sa.DATE(), autoincrement=False, nullable=False),
        sa.Column("end", sa.DATE(), autoincrement=False, nullable=True),
        sa.Column("temporary", sa.BOOLEAN(), autoincrement=False, nullable=False),
        sa.PrimaryKeyConstraint("id", name="pk_network"),
        schema="psd",
        postgresql_ignore_search_path=False,
    )
    op.create_index("ix_network_year", "network", ["year"], unique=False, schema="psd")
    op.create_index(
        "ix_network_start", "network", ["start"], unique=False, schema="psd"
    )
    op.create_index("ix_network_code", "network", ["code"], unique=False, schema="psd")

    # Source
    op.create_table(
        "source",
        sa.Column("id", sa.UUID(), autoincrement=False, nullable=False),
        sa.Column("network_id", sa.UUID(), autoincrement=False, nullable=False),
        sa.Column("station", sa.VARCHAR(), autoincrement=False, nullable=False),
        sa.Column("location", sa.VARCHAR(), autoincrement=False, nullable=True),
        sa.Column("channel", sa.VARCHAR(), autoincrement=False, nullable=False),
        sa.ForeignKeyConstraint(
            ["network_id"],
            ["psd.network.id"],
            name="fk_source_network_id_network",
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name="pk_source"),
        schema="psd",
        postgresql_ignore_search_path=False,
    )
    op.create_index(
        "ix_source_station", "source", ["station"], unique=False, schema="psd"
    )
    op.create_index(
        "ix_source_network", "source", ["network_id"], unique=False, schema="psd"
    )
    op.create_index(
        "ix_source_location", "source", ["location"], unique=False, schema="psd"
    )
    op.create_index(
        "ix_source_channel", "source", ["channel"], unique=False, schema="psd"
    )

    # File
    op.create_table(
        "file",
        sa.Column("id", sa.UUID(), autoincrement=False, nullable=False),
        sa.Column("source_id", sa.UUID(), autoincrement=False, nullable=True),
        sa.Column("date", sa.DATE(), autoincrement=False, nullable=False),
        sa.Column("year", sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column("day", sa.INTEGER(), autoincrement=False, nullable=False),
        sa.Column("name", sa.VARCHAR(), autoincrement=False, nullable=False),
        sa.Column("processed", sa.BOOLEAN(), autoincrement=False, nullable=False),
        sa.Column("metadata", sa.BOOLEAN(), autoincrement=False, nullable=False),
        sa.Column(
            "date_created", postgresql.TIMESTAMP(), autoincrement=False, nullable=True
        ),
        sa.Column(
            "date_processed", postgresql.TIMESTAMP(), autoincrement=False, nullable=True
        ),
        sa.ForeignKeyConstraint(
            ["source_id"],
            ["psd.source.id"],
            name="fk_file_source_id_source",
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name="pk_file"),
        sa.UniqueConstraint("name", name="uq_file_name"),
        schema="psd",
    )
    op.create_index("ix_file_source", "file", ["source_id"], unique=False, schema="psd")
    op.create_index("ix_file_date", "file", ["date"], unique=False, schema="psd")

    # Epoch
    op.create_table(
        "epoch",
        sa.Column("id", sa.UUID(), autoincrement=False, nullable=False),
        sa.Column("source_id", sa.UUID(), autoincrement=False, nullable=False),
        sa.Column("start", postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
        sa.Column("end", postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
        sa.Column(
            "instrument",
            postgresql.JSON(astext_type=sa.Text()),
            autoincrement=False,
            nullable=True,
        ),
        sa.ForeignKeyConstraint(
            ["source_id"],
            ["psd.source.id"],
            name="fk_epoch_source_id_source",
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name="pk_epoch"),
        schema="psd",
    )
    op.create_index(
        "ix_epoch_source", "epoch", ["source_id"], unique=False, schema="psd"
    )

    # Stream
    op.create_table(
        "stream",
        sa.Column("id", sa.UUID(), autoincrement=False, nullable=False),
        sa.Column("file_id", sa.UUID(), autoincrement=False, nullable=False),
        sa.Column("source_id", sa.UUID(), autoincrement=False, nullable=False),
        sa.Column("epoch_id", sa.UUID(), autoincrement=False, nullable=True),
        sa.Column(
            "valid",
            sa.BOOLEAN(),
            server_default=sa.text("true"),
            autoincrement=False,
            nullable=False,
        ),
        sa.Column("start", postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
        sa.Column("end", postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
        sa.Column(
            "sampling_rate",
            sa.DOUBLE_PRECISION(precision=53),
            autoincrement=False,
            nullable=False,
        ),
        sa.ForeignKeyConstraint(
            ["epoch_id"],
            ["psd.epoch.id"],
            name="fk_stream_epoch_id_epoch",
            onupdate="CASCADE",
            ondelete="SET NULL",
        ),
        sa.ForeignKeyConstraint(
            ["file_id"],
            ["psd.file.id"],
            name="fk_stream_file_id_file",
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["source_id"],
            ["psd.source.id"],
            name="fk_stream_source_id_source",
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name="pk_stream"),
        schema="psd",
        postgresql_ignore_search_path=False,
    )
    op.create_index(
        "ix_stream_source", "stream", ["source_id"], unique=False, schema="psd"
    )
    op.create_index("ix_stream_file", "stream", ["file_id"], unique=False, schema="psd")
    op.create_index(
        "ix_stream_epoch", "stream", ["epoch_id"], unique=False, schema="psd"
    )

    # Trace
    op.create_table(
        "trace",
        sa.Column("stream_id", sa.UUID(), autoincrement=False, nullable=False),
        sa.Column(
            "timestamp", postgresql.TIMESTAMP(), autoincrement=False, nullable=False
        ),
        sa.Column(
            "data",
            postgresql.ARRAY(sa.DOUBLE_PRECISION(precision=53)),
            autoincrement=False,
            nullable=True,
        ),
        sa.ForeignKeyConstraint(
            ["stream_id"],
            ["psd.stream.id"],
            name="fk_trace_stream_id_stream",
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("stream_id", "timestamp", name="pk_trace"),
        schema="psd",
    )
    op.create_index(
        "ix_trace_stream", "trace", ["stream_id"], unique=False, schema="psd"
    )

    # Gap
    op.create_table(
        "gap",
        sa.Column("stream_id", sa.UUID(), autoincrement=False, nullable=False),
        sa.Column("start", postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
        sa.Column("end", postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
        sa.ForeignKeyConstraint(
            ["stream_id"],
            ["psd.stream.id"],
            name="fk_gap_stream_id_stream",
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("stream_id", "start", name="pk_gap"),
        schema="psd",
    )
    op.create_index("ix_gap_stream", "gap", ["stream_id"], unique=False, schema="psd")

    return table_network


def downgrade_shared(connection):
    logging.info("Migrating schema: shared")

    # Create psd schema
    psd_network = create_schema_psd(connection)

    # Migrate networks
    items = []
    for network in connection.execute(sa.text('select * from "shared"."network"')):
        items.append(
            {
                "id": network.id,
                "code": network.code,
                "year": network.year,
                "start": network.start,
                "end": network.end,
                "temporary": network.temporary,
            }
        )
    op.bulk_insert(psd_network, items)

    # Migrate sources
    connection.execute(
        sa.text('INSERT INTO "psd"."source" (SELECT * FROM "shared"."source")')
    )

    # Migrate files
    connection.execute(
        sa.text('INSERT INTO "psd"."file" (SELECT * FROM "shared"."file")')
    )


def downgrade_tenant(connection, schema, network_id):
    logging.info(f"Migrating schema: {schema}")

    # Migrate epochs
    connection.execute(
        sa.text(
            f'INSERT INTO "psd"."epoch" (SELECT epoch.* FROM "{schema}"."epoch" JOIN "shared"."source" ON epoch.source_id = source.id WHERE source.network_id = :network_id)'
        ),
        {"network_id": network_id},
    )

    # Migrate streams
    connection.execute(
        sa.text(
            f'INSERT INTO "psd"."stream" (SELECT stream.* FROM "{schema}"."stream" JOIN "shared"."source" ON stream.source_id = source.id WHERE source.network_id = :network_id)'
        ),
        {"network_id": network_id},
    )

    # Migrate traces
    connection.execute(
        sa.text(
            f'INSERT INTO "psd"."trace" (SELECT trace.* FROM "{schema}"."trace" JOIN "{schema}"."stream" ON trace.stream_id = stream.id JOIN "shared"."source" ON stream.source_id = source.id WHERE source.network_id = :network_id)'
        ),
        {"network_id": network_id},
    )

    # Migrate gaps
    connection.execute(
        sa.text(
            f'INSERT INTO "psd"."gap" (SELECT gap.* FROM "{schema}"."gap" JOIN "{schema}"."stream" ON gap.stream_id = stream.id JOIN "shared"."source" ON stream.source_id = source.id WHERE source.network_id = :network_id)'
        ),
        {"network_id": network_id},
    )

    # Delete old data related
    # connection.execute(sa.text(f'DELETE from "shared"."source" where source.network_id = :network_id'), {'network_id': network_id})
