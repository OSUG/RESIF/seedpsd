import verboselogs
from pyramid.view import view_config

from seedpsd import __version__

logger = verboselogs.VerboseLogger(__name__)


@view_config(route_name="version", renderer="string")
class VersionView:
    def __init__(self, request):
        logger.debug(request)
        self.request = request

    def __call__(self):
        self.request.response.content_type = "text/plain"
        return __version__
