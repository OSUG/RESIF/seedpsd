from pyramid.httpexceptions import HTTPException, HTTPNoContent, HTTPNotFound

from seedpsd.api.views import BaseView
from seedpsd.errors import InvalidParameterValueError
from seedpsd.tools.common import str2bool, to_datetime


class QueryView(BaseView):
    @property
    def worker_ppsd(self):
        return self.request.registry.get("worker_ppsd")

    @property
    def worker_content(self):
        return self.request.registry.get("worker_content")

    @property
    def worker_value(self):
        return self.request.registry.get("worker_value")

    # MANDATORY PARAMETERS #############################################################################################

    @property
    def network(self):
        return self._get_parameter("network", ["net"], mandatory=True)

    @property
    def station(self):
        return self._get_parameter("station", ["sta"], mandatory=True)

    @property
    def location(self):
        loc = self._get_parameter("location", ["loc"], mandatory=True)
        if loc == "--":
            return ""
        return loc

    @property
    def channel(self):
        return self._get_parameter("channel", ["cha"], mandatory=True)

    @property
    def start_time(self):
        start = self._get_parameter(
            "start", ["starttime", "start_time"], mandatory=True
        )
        try:
            return to_datetime(start)
        except Exception:
            msg = "start"
            raise InvalidParameterValueError(msg, start)

    @property
    def end_time(self):
        end = self._get_parameter("end", ["endtime", "end_time"], mandatory=True)
        try:
            return to_datetime(end)
        except Exception:
            msg = "end"
            raise InvalidParameterValueError(msg, end)

    @property
    def outdated(self):
        return str2bool(self._get_parameter("outdated", default=True))

    # METHODS ##########################################################################################################

    def get_sources(self):
        """
        Get the sources using the request parameters
        :return: A list of Source objects
        """
        try:
            return self.worker_ppsd.get_sources(
                self.network,
                self.station,
                self.location,
                self.channel,
                self.start_time,
                self.end_time,
            )
        except HTTPException:
            raise
        except Exception as e:
            nodata = self.request.params.get("nodata", None)
            if nodata is not None:
                raise HTTPNotFound(str(e))
            raise HTTPNoContent(str(e))

    def get_ppsds(self, source):
        """
        Get the PPSD objects for a Source
        :param source: The Source object
        :return: A list of PPSD objects
        """
        try:
            return self.worker_ppsd.from_database(
                source, self.start_time, self.end_time, with_outdated=self.outdated
            )
        except HTTPException:
            raise
        except Exception:
            raise
