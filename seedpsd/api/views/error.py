from pyramid.httpexceptions import HTTPException
from pyramid.view import exception_view_config


@exception_view_config(
    HTTPException, renderer="seedpsd:api/templates/views/error.jinja2"
)
def error_view(exc, request):
    request.response.status = exc.code
    debug = request.registry.get("settings").log_stacktrace
    return {"error": exc, "debug": debug}
