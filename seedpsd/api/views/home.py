import verboselogs
from pyramid.view import view_config

from . import BaseView

logger = verboselogs.VerboseLogger(__name__)


@view_config(route_name="home", renderer="seedpsd:api/templates/views/home.jinja2")
class HomeView(BaseView):
    def __call__(self):
        logger.debug(self.request)
        self.request._LOCALE_ = self.locale
        return {
            "locale": self.locale,
            "command": "home",
        }
