import click

from .commands.admin import admin
from .commands.content import content
from .commands.data import data
from .commands.locale import locale
from .commands.metadata import metadata
from .commands.psd import psd
from .commands.server import server


# Declare the main command
@click.group(
    help="Welcome to SeedPSD command-line interface. Please use one of the following commands."
)
def cli():
    pass


cli.add_command(admin)
cli.add_command(content)
cli.add_command(data)
cli.add_command(locale)
cli.add_command(metadata)
cli.add_command(psd)
cli.add_command(server)

if __name__ == "__main__":
    cli()
