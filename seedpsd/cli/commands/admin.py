import logging
import os
import sys

import alembic.command
import click
from click_aliases import ClickAliasedGroup
from sqlalchemy import create_engine, inspect, text
from sqlalchemy.schema import CreateSchema, DropSchema
from sqlalchemy_utils.functions import create_database, database_exists, drop_database

from seedpsd.cli.tools import configure_logger, configure_sentry, log_levels
from seedpsd.settings.alembic import AlembicSettings
from seedpsd.settings.workers import Settings

# CONFIGURATION
########################################################################################################################


# Group all sub-commands declared below
@click.group(help="Database administration", cls=ClickAliasedGroup)
def admin():
    pass


@admin.command(help="Show the configuration")
@click.option("--pretty", is_flag=True, help="Pretty print")
@click.option(
    "--level",
    "log_level",
    type=log_levels,
    default=logging.INFO,
    help="Log verbosity level",
)
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def config(pretty, log_level, sql_log_level, debug, config_path):
    # Loading settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        if pretty:
            env_config = "\n".join(
                f"{key}: {value}" for key, value in settings.env_config.items()
            )
            full_config = "\n".join(
                f"{key}: {value}" for key, value in settings.full_config.items()
            )
        else:
            env_config = settings.env_config
            full_config = settings.full_config

        logger.info(f"Environment config:\n{env_config}\n")
        logger.info(f"Application config:\n{full_config}\n")

        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Display settings failed")
        sys.exit(os.EX_SOFTWARE)


# CREATION
########################################################################################################################


@admin.command(
    help="Create the database and/or its structure",
    aliases=["database-create", "schema-create"],
)
@click.option("--encoding", default="utf8", help="Database encoding (default: utf8)")
@click.option(
    "--template", default="template0", help="Database template (default: template0)"
)
@click.option(
    "--level",
    "log_level",
    type=log_levels,
    default=logging.INFO,
    help="Log verbosity level",
)
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def create(encoding, template, log_level, sql_log_level, debug, config_path):
    # Loading settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Initialize the database engine
        engine = create_engine(settings.database_uri)

        # First, create the database if needed
        if not database_exists(engine.url):
            logger.info("Creating the database...")
            try:
                create_database(engine.url, encoding=encoding, template=template)
            except Exception:
                logger.exception(
                    "Database creation failed. Please use an already created database name or a PostgreSQL username with administrative rights."
                )
                raise

        # Create the structure
        logger.info("Creating the 'shared' schema...")
        with engine.begin() as db:
            try:
                # Create shared schema
                db.execute(CreateSchema("shared", if_not_exists=True))

                # Create shared tables
                from seedpsd.models.db.base import get_shared_metadata

                get_shared_metadata().create_all(bind=db)

                # Apply the Alembic version number
                alembic_cfg = AlembicSettings(settings)
                alembic.command.stamp(alembic_cfg, "head", purge=True)
            except Exception as e:
                click.echo(str(e))
                raise

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("DB creation failed")
        sys.exit(os.EX_SOFTWARE)


# DELETION
########################################################################################################################


@admin.command(
    help="Drop the database and/or its structure",
    aliases=["database-drop", "schema-drop", "delete"],
)
@click.option("--schema", help="Name of a schema to drop")
@click.option(
    "--force",
    is_flag=True,
    prompt="Do you want to drop the database structure?",
    help="Force the deletion without manual confirmation",
)
@click.option(
    "--level",
    "log_level",
    type=log_levels,
    default=logging.INFO,
    help="Log verbosity level",
)
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def drop(schema, force, log_level, sql_log_level, debug, config_path):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Prevent unwanted deletion by forcing a flag
        if not force:
            click.echo("Operation aborted.")
            sys.exit(os.EX_OK)
        else:
            # Initialize the database engine
            engine = create_engine(settings.database_uri, isolation_level="AUTOCOMMIT")

            # Does the database exist?
            if not database_exists(engine.url):
                click.echo("ERROR: The database doesn't exist")
                sys.exit(os.EX_OK)

            # Schema deletion
            if schema:
                inspector = inspect(engine)
                with engine.connect() as connection:
                    for s in schema.split(","):
                        if (
                            s not in ["information_schema", "public"]
                            and s in inspector.get_schema_names()
                        ):
                            connection.execute(
                                DropSchema(schema, cascade=True, if_exists=True)
                            )
                            click.echo(f"Schema {s} dropped.")

            # Database / all schemas deletion
            else:
                # With admin rights
                try:
                    drop_database(engine.url)
                    click.echo("DONE: Database dropped.")
                    sys.exit(os.EX_OK)
                except Exception:
                    logger.exception("Error while dropping database")

                # Without admin rights
                inspector = inspect(engine)
                with engine.connect() as connection:
                    logger.info("Dropping the schemas...")
                    for schema in inspector.get_schema_names():
                        if schema not in ["information_schema", "public"]:
                            try:
                                connection.execute(
                                    DropSchema(schema, cascade=True, if_exists=True)
                                )
                                logger.info(f"Schema '{schema}' deleted")
                            except Exception:
                                logger.warning(f"Schema '{schema}' deletion failed")

                    logger.info("Dropping the Alembic table...")
                    try:
                        connection.execute(
                            text("DROP TABLE IF EXISTS alembic_version ;")
                        )
                    except Exception:
                        logger.warning("Alembic table deletion failed")

                    click.echo("Schemas dropped.")

            logger.info("Done.")
            sys.exit(os.EX_OK)

    except Exception:
        logger.exception("DB drop failed")
        sys.exit(os.EX_SOFTWARE)


# # ALEMBIC WRAPPER
# ########################################################################################################################


@admin.command(
    help="Show/Update the database schema version", aliases=["schema-version"]
)
@click.option("--stamp", help="Stamp the database schema to a specific version")
@click.option("--current", is_flag=True, help="Show current version")
@click.option(
    "--level",
    "log_level",
    type=log_levels,
    default=logging.INFO,
    help="Log verbosity level",
)
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def version(stamp, current, log_level, sql_log_level, debug, config_path):
    # Loading settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Initialize the database engine
        engine = create_engine(settings.database_uri)

        # Check if the database exists
        if not database_exists(engine.url):
            click.echo("ERROR: The database doesn't exist")

        else:
            # Configure Alembic
            alembic_cfg = AlembicSettings(settings)

            # Get DB history
            if current:
                alembic.command.current(alembic_cfg)
            elif stamp:
                alembic.command.stamp(alembic_cfg, stamp)
                click.echo(f"DONE: Database stamped to version '{stamp}'.")
            else:
                alembic.command.history(alembic_cfg, indicate_current=True)

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("DB version display failed")
        sys.exit(os.EX_SOFTWARE)


@admin.command(
    help="Apply/Generate a database schema migration", aliases=["schema-migrate"]
)
@click.option(
    "--upgrade",
    "action",
    flag_value="upgrade",
    help="Upgrade database schema up to a version",
)
@click.option(
    "--downgrade",
    "action",
    flag_value="downgrade",
    help="Downgrade database schema up to a version",
)
@click.option(
    "--autogenerate",
    "action",
    flag_value="autogenerate",
    help="Generate a database schema migration",
)
@click.option("--version", default="head", help="Target version (default=head)")
@click.option(
    "--level",
    "log_level",
    type=log_levels,
    default=logging.INFO,
    help="Log verbosity level",
)
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def migrate(action, version, log_level, sql_log_level, debug, config_path):
    # Loading settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Initialize the database engine
        engine = create_engine(settings.database_uri)

        # Check if the database exists
        if not database_exists(engine.url):
            click.echo("ERROR: The database doesn't exist")

        else:
            # Configure Alembic
            alembic_cfg = AlembicSettings(settings)

            # Upgrade DB through Alembic
            if action and action == "upgrade":
                click.echo(f"Upgrading the database structure to {version}...")
                alembic.command.upgrade(alembic_cfg, version)
                click.echo(f"DONE: Database upgraded to {version}.")
            elif action and action == "downgrade":
                click.echo(f"Downgrading the database structure to {version}...")
                alembic.command.downgrade(alembic_cfg, version)
                click.echo(f"DONE: Database downgraded to {version}.")
            elif action and action == "autogenerate":
                click.echo("Building a new database migration template...")
                alembic.command.revision(alembic_cfg, autogenerate=True)
                click.echo("DONE: New revision created.")
            else:
                click.echo("Comparing the database structure with the data model...")
                alembic.command.check(alembic_cfg)

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Schema migration failed")
        sys.exit(os.EX_SOFTWARE)
