import os

import verboselogs
from obspy.signal import PPSD

from . import SeedPsdFile

logger = verboselogs.VerboseLogger(__name__)


# NPZ FILE #############################################################################################################
########################################################################################################################


class NPZFile(SeedPsdFile):
    """
    NPZFile class
    """

    def __init__(
        self,
        year,
        network,
        station,
        location,
        channel,
        quality="D",
        file_name=None,
        parent_path=None,
    ):
        """
        Initialize an NPZFile

        :param year: The year
        :param network: The network code
        :param station: The station code
        :param location: The location code
        :param channel: The channel code
        :param quality: The quality code (fixed to 'D')
        :rtype: NPZFile
        """
        logger.debug(
            f"NPZFile.__init__({year}, {network}, {station}, {location}, {channel}, {quality}, {file_name}, {parent_path})"
        )
        super().__init__(
            year, network, station, location, channel, quality, file_name, parent_path
        )

    def __repr__(self):
        return f"<NPZFile {self._id}>"

    # PROPERTIES #######################################################################################################

    @property
    def _id(self):
        """
        Generate the NPZ identifier

        :rtype: str
        """
        return f"{self.network}.{self.station}.{self.location}.{self.channel}.{self.quality}.{self.year}"

    @property
    def _generated_file_name(self):
        """
        Generate the NPZ file name

        :rtype: str
        """
        return f"{self._id}.npz"

    # LOAD #############################################################################################################

    def ppsd(self, metadata):
        """
        Build a PPSD object from the NPZ

        :param metadata: The metadata/inventory
        :rtype: PPSD
        """
        try:
            return PPSD.load_npz(self.file_path, metadata=metadata)
        except Exception as e:  # FIXME
            logger.debug(e)
            return None

    # PATH RELATED METHODS #############################################################################################

    @property
    def is_dirty(self):
        """
        Check if the NPZ file is dirty

        :rtype: bool
        """
        return os.path.exists(str(self.file_path).replace(".npz", ".dirty"))

    # def _dir_path(self, npz_dir):
    #     """
    #     Generate the NPZ directory path
    #
    #     :param npz_dir: The NPZ directory
    #     :rtype: str
    #     """
    #     return os.path.join(npz_dir, self._network, self._year)
    #
    # def _file_path(self, npz_dir):
    #     """
    #     Generate the NPZ file path
    #
    #     :param npz_dir: The NPZ directory
    #     :rtype: str
    #     """
    #     return os.path.join(self._dir_path(npz_dir), self._file_name)
