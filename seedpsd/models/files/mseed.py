import pendulum
import verboselogs
from obspy import read

from . import SeedPsdFile

logger = verboselogs.VerboseLogger(__name__)


# MSEED FILE ###########################################################################################################
########################################################################################################################


class MSEEDFile(SeedPsdFile):
    """
    MSEEDFile class
    """

    def __init__(
        self,
        day,
        year,
        network,
        station,
        location,
        channel,
        quality="D",
        file_name=None,
        parent_path=None,
    ):
        """
        Initialize a MSEEDFile object

        :param day: The day
        :param year: The year
        :param network: The network code
        :param station: The station code
        :param location: The location code
        :param channel: The channel code
        :param quality: The channel quality (fixed to 'D')
        :param file_name: The file name (Optional)
        :param parent_path: The parent path (Optional)
        :rtype: MSEEDFile
        """
        logger.debug(
            f"MSEEDFile.__init__({day}, {year}, {network}, {station}, {location}, {channel}, {quality}, {file_name}, {parent_path})"
        )
        super().__init__(
            year, network, station, location, channel, quality, file_name, parent_path
        )
        self._day = day

    def __repr__(self):
        return f"<MSEEDFile {self._id}>"

    # PROPERTIES #######################################################################################################

    @property
    def day(self):
        """
        Generate the day on 3 characters (filled with 0)

        :rtype: str
        """
        return str(self._day).zfill(3)

    @property
    def _id(self):
        """
        Build the MSEED identifier

        :rtype: str
        """
        return f"{self.network}.{self.station}.{self.location or ''}.{self.channel}.{self.quality}.{self.year}.{self.day}"

    @property
    def _generated_file_name(self):
        """
        Generate the filename

        :rtype: str
        """
        return self._id

    @property
    def date(self):
        if self.year and self.day:
            return pendulum.from_format(f"{self.year}-{self.day}", "YYYY-DDDD").date()
        return None

    # ACTIONS ##########################################################################################################

    def read(self, raise_error=False):
        """
        Read the file from the specified directory

        :return: An ObsPy :class:`~obspy.core.stream.Stream` object.
        """
        logger.debug(f"MSEEDFile.read({raise_error})")
        try:
            logger.debug(f"Opening file {self.file_path}")
            return read(self.file_path, format="MSEED")
        except Exception as e:
            logger.debug(e)
            if raise_error:
                raise
            return None
