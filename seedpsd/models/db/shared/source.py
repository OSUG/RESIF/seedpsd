import uuid

import verboselogs
from sqlalchemy import (
    Column,
    ForeignKey,
    Index,
    PrimaryKeyConstraint,
    String,
    UniqueConstraint,
    and_,
    null,
    or_,
    select,
)
from sqlalchemy.orm import relationship
from sqlalchemy_utils import UUIDType

from seedpsd.models.db.base import Base
from seedpsd.models.db.shared.network import Network
from seedpsd.tools.common import to_datetime

__all__ = ("Source",)
logger = verboselogs.VerboseLogger(__name__)


class Source(Base):
    __tablename__ = "source"
    __table_args__ = (
        PrimaryKeyConstraint("id"),
        UniqueConstraint("network_id", "station", "location", "channel"),
        Index("ix_source_network", "network_id"),
        Index("ix_source_station", "station"),
        Index("ix_source_location", "location"),
        Index("ix_source_channel", "channel"),
        {"schema": "shared"},
    )
    id = Column(UUIDType(), default=uuid.uuid4)

    network_id = Column(
        ForeignKey("shared.network.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    network = relationship("Network", back_populates="sources", lazy="joined")

    station = Column(String, nullable=False)
    location = Column(String, nullable=False)
    channel = Column(String, nullable=False)

    streams = relationship(
        "Stream",
        back_populates="source",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    files = relationship(
        "File",
        back_populates="source",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    epochs = relationship(
        "Epoch",
        back_populates="source",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )

    @property
    def nslc(self):
        """
        Get the NSLC of the source
        :rtype: str
        """
        return (
            f"{self.network.code}.{self.station}.{self.location or ''}.{self.channel}"
        )

    def __repr__(self):
        return f"<Source {self.id} | {self.nslc} | {self.network.extended_code}>"

    def __init__(self, network, station, location, channel):
        """
        Create a Source object
        :param network: The network code
        :param station: The station code
        :param location: The location code
        :param channel: The channel code
        """
        logger.debug(f"Source({network}, {station}, {location}, {channel})")
        self.id = uuid.uuid4()
        self.network = network
        self.station = station
        self.channel = channel
        self.location = location

    # SQL STATEMENT BUILDING ###########################################################################################

    @classmethod
    def find_id(
        cls,
        network=None,
        station=None,
        location=None,
        channel=None,
        start=None,
        end=None,
    ):
        """
        Build a statement for selecting ID of sources
        :param network: Filter by network code
        :param station: Filter by station code
        :param location: Filter by location code
        :param channel: Filter by channel code
        :param start: Filter by start datetime
        :param end: Filter by end datetime
        :return: Select statement
        """
        logger.debug(
            f"Source.find_id({network}, {station}, {location}, {channel}, {start}, {end})"
        )
        statement = select(cls.id)
        return cls.find(network, station, location, channel, start, end, statement)

    @classmethod
    def find(
        cls,
        network=None,
        station=None,
        location=None,
        channel=None,
        start=None,
        end=None,
        statement=None,
    ):
        """
        Build a statement for selecting sources
        :param network: Filter by network code
        :param station: Filter by station code
        :param location: Filter by location code
        :param channel: Filter by channel code
        :param start: Filter by start datetime
        :param end: Filter by end datetime
        :param statement: An initialized statement
        :return: Select statement
        """
        logger.debug(
            f"Source.find({network}, {station}, {location}, {channel}, {start}, {end}, {statement})"
        )

        if statement is None:
            statement = select(cls)

        # Build JOIN part of statement
        statement = statement.outerjoin(Network, Source.network)

        # Build WHERE part of statement
        # Initialize a container for WHERE conditions
        params = []

        # Build WHERE conditions
        if network is not None and network != "*":
            if isinstance(network, Network):
                params.append(cls.network == network)
            elif isinstance(network, uuid.UUID):
                params.append(cls.network_id == network)
            elif isinstance(network, (list, tuple, set)):
                ids = []
                for net in network:
                    if isinstance(net, Network):
                        ids.append(net.id)
                    elif isinstance(net, uuid.UUID):
                        ids.append(net)
                params.append(cls.network_id.in_(ids))
            else:
                params.append(Network._where_wildcard(network, Network.code))

        if station is not None and station != "*":
            params.append(cls._where_wildcard(station, cls.station))

        if location is not None and location != "*":
            params.append(cls._where_wildcard(location, cls.location))

        if channel is not None and channel != "*":
            params.append(cls._where_wildcard(channel, cls.channel))

        if start is not None:
            params.append(
                or_(Network._end >= to_datetime(start), Network._end == null())
            )
        if end is not None:
            params.append(Network.start <= to_datetime(end))

        # Add the WHERE conditions to the SQL statement
        if params:
            statement = (
                statement.where(and_(*params))
                if len(params) > 1
                else statement.where(*params)
            )

        # Build ORDER BY part of statement
        statement = statement.order_by(
            Network.code, Network.year, cls.station, cls.location, cls.channel
        )
        logger.debug(statement)

        return statement

    # LOAD FROM DB OR CREATE ###########################################################################################

    @classmethod
    def factory(
        cls,
        db_session,
        network=None,
        station=None,
        location="",
        channel=None,
        start=None,
        end=None,
        add_session=True,
    ):
        """
        Load from database or create a Source object
        :param db_session: The database session
        :param network: The network code
        :param station: The station code
        :param location: The location code
        :param channel: The channel code
        :param start: The start datetime
        :param end: The end datetime
        :param add_session: Add the created object to the DB session
        :rtype: Source
        """
        logger.debug(
            f"Source.factory({db_session}, {network}, {station}, {location}, {channel}, {start}, {end}, {add_session})"
        )
        source = db_session.find_one(
            cls.find(network, station, location, channel, start, end)
        )
        if not source:
            logger.debug("> Creating new Source")
            source = cls(network, station, location, channel)
            if add_session:
                db_session.add(source)
        return source
