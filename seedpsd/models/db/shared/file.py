import uuid

import pendulum
import verboselogs
from obspy import UTCDateTime
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    DateTime,
    ForeignKey,
    Index,
    Integer,
    PrimaryKeyConstraint,
    String,
    UniqueConstraint,
    and_,
    delete,
    or_,
    select,
)
from sqlalchemy.orm import relationship
from sqlalchemy_utils import UUIDType

from seedpsd.models.db.base import Base
from seedpsd.tools.common import datetime_now, to_date

from .source import Source

__all__ = ("File",)
logger = verboselogs.VerboseLogger(__name__)


class File(Base):
    __tablename__ = "file"
    __table_args__ = (
        PrimaryKeyConstraint("id"),
        UniqueConstraint("name"),
        Index("ix_file_source", "source_id"),
        Index("ix_file_date", "date"),
        {"schema": "shared"},
    )

    id = Column(UUIDType(), default=uuid.uuid4)  # FIXME

    source_id = Column(
        ForeignKey("shared.source.id", onupdate="CASCADE", ondelete="CASCADE")
    )  # FIXME
    source = relationship("Source", back_populates="files")

    date = Column(Date, nullable=False)
    year = Column(Integer, nullable=False)
    day = Column(Integer, nullable=False)

    name = Column(String, nullable=False)
    processed = Column(Boolean, nullable=False, default=False)
    available_metadata = Column("metadata", Boolean, nullable=False, default=True)

    date_created = Column(DateTime())
    date_processed = Column(DateTime())

    streams = relationship(
        "Stream",
        back_populates="file",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )

    def __repr__(self):
        return f"<File {self.id} | {self.source.nslc if not self.is_detached and self.source else self.source_id} | {self.date} | {self.name} | Processed: {self.processed} | Metadata: {self.available_metadata}>"

    def __init__(
        self,
        name=None,
        date=None,
        year=None,
        day=None,
        processed=None,
        available_metadata=None,
    ):
        """
        Create a File object
        :param name: The file name
        :param date: The date
        :param year: The year
        :param day: The day
        :param state: The state
        :rtype: File
        """
        logger.debug(
            f"File({name}, {date}, {year}, {day}, {processed}, {available_metadata})"
        )
        self.id = uuid.uuid4()
        self.date_created = datetime_now()

        if date is not None:
            self.date = to_date(date)
            self.year = self.date.year
            self.day = int(self.date.strftime("%j"))
        elif year is not None and day is not None:
            self.year = int(year)
            self.day = int(day)
            self.date = pendulum.from_format(
                f"{year}-{str(day).zfill(3)}", "YYYY-DDDD"
            ).date()

        if name is not None:
            self.name = name

        if processed is not None:
            self.processed = processed

        if available_metadata is not None:
            self.available_metadata = available_metadata

    def invalidate(self):
        """
        Invalidate the file
        """
        logger.debug("File.invalidate()")

        # Flag the file as invalid
        self.processed = False

        return True

    @property
    def date_processed_utc(self):
        """
        Get the processed datetime as an UTCDateTime
        :rtype: UTCDateTime
        """
        return UTCDateTime(self.date_processed)

    # SQL STATEMENT BUILDING ###########################################################################################

    @classmethod
    def delete(
        cls,
        source=None,
        date=None,
        year=None,
        day=None,
        name=None,
        before=None,
        after=None,
        dirty=None,
        orphan=None,
    ):
        """
        Build a statement for deleting files
        :param source: Filter file related to a source object
        :param date: Filter file by date
        :param year: Filter file by year
        :param day: Filter file by day
        :param name: Filter file by name
        :param before: Filter file before a datetime
        :param after: Filter file after a datetime
        :param dirty: Filter dirty files
        :param orphan: Filter orphan files
        :return: Select statement
        """
        logger.debug(
            f"File.delete({source}, {date}, {year}, {day}, {name}, {before}, {after}, {dirty}, {orphan})"
        )
        statement = delete(cls)
        return cls.find(
            source,
            date,
            year,
            day,
            name,
            before,
            after,
            dirty,
            orphan,
            statement,
            ordered=False,
        )

    @classmethod
    def find_id(
        cls,
        source=None,
        date=None,
        year=None,
        day=None,
        name=None,
        before=None,
        after=None,
        dirty=None,
        orphan=None,
    ):
        """
        Build a statement for selecting ID of files
        :param source: Filter file related to a source object
        :param date: Filter file by date
        :param year: Filter file by year
        :param day: Filter file by day
        :param name: Filter file by name
        :param before: Filter file before a datetime
        :param after: Filter file after a datetime
        :param dirty: Filter dirty files
        :param orphan: Filter orphan files
        :return: Select statement
        """
        logger.debug(
            f"File.find_id({source}, {date}, {year}, {day}, {name}, {before}, {after}, {dirty}, {orphan})"
        )
        statement = select(cls.id)
        return cls.find(
            source, date, year, day, name, before, after, dirty, orphan, statement
        )

    @classmethod
    def find(
        cls,
        source=None,
        date=None,
        year=None,
        day=None,
        name=None,
        before=None,
        after=None,
        dirty=None,
        orphan=None,
        statement=None,
        ordered=True,
    ):
        """
        Build a statement for selecting files
        :param source: Filter file related to a source object
        :param date: Filter file by date
        :param year: Filter file by year
        :param day: Filter file by day
        :param name: Filter file by name
        :param before: Filter file before a datetime
        :param after: Filter file after a datetime
        :param dirty: Filter dirty files
        :param orphan: Filter orphan files
        :param statement: An initialized statement
        :return: Select statement
        """
        logger.debug(
            f"File.find({source}, {date}, {year}, {day}, {name}, {before}, {after}, {dirty}, {orphan}, {statement})"
        )

        if statement is None:
            statement = select(cls)

        params = []

        if source:
            if isinstance(source, Source):
                params.append(cls.source == source)
            elif isinstance(source, uuid.UUID):
                params.append(cls.source_id == source)
            elif isinstance(source, (list, tuple, set)):
                ids = []
                for src in source:
                    if isinstance(src, Source):
                        ids.append(src.id)
                    elif isinstance(src, uuid.UUID):
                        ids.append(src)
                params.append(cls.source_id.in_(ids))

        if name:
            if isinstance(name, (list, tuple, set)):
                params.append(cls.name.in_(list(name)))
            elif "%" in name:
                params.append(cls.name.like(name))
            else:
                params.append(cls.name == name)

        if date:
            params.append(cls.date == to_date(date))
        if before:
            params.append(cls.date < to_date(before))
        if after:
            params.append(cls.date > to_date(after))

        if year:
            if isinstance(year, (list, tuple, set)):
                params.append(cls.year.in_(list(year)))
            else:
                params.append(cls.year == year)

        if day is not None:
            if isinstance(day, (list, tuple, set)):
                params.append(cls.day.in_(list(day)))
            else:
                params.append(cls.day == day)

        if dirty:
            params.append(
                or_(
                    cls.processed == False,  # noqa: E712
                    cls.available_metadata == False,  # noqa: E712
                )
            )

        if orphan:
            params.append(cls.source_id.is_(None))
        elif orphan is not None:
            params.append(cls.source_id.is_not(None))

        # Add the WHERE conditions to the SQL statement
        if params:
            statement = (
                statement.where(and_(*params))
                if len(params) > 1
                else statement.where(*params)
            )

        # Build ORDER BY part of statement
        if ordered:
            statement = statement.order_by(cls.date)
            # statement = statement.order_by(Network.code, Network.year, Source.station, Source.location, Source.channel, File.date)

        return statement

    # LOAD FROM DB OR CREATE ###########################################################################################

    @classmethod
    def factory(
        cls,
        db_session,
        source=None,
        year=None,
        day=None,
        name=None,
        processed=None,
        metadata=None,
        add_session=True,
    ):
        """
        Load from database or create a File object
        :param db_session: The database session
        :param source: The related source object
        :param year: The year
        :param day: The day
        :param name: The file name
        :param processed: The file has been processed
        :param metadata: The related metadata are available
        :param add_session: Add the created object to the DB session
        :return: A File object
        :rtype: File
        """
        logger.debug(
            f"File.factory({db_session}, {source}, {year}, {day}, {name}, {processed}, {metadata}, {add_session})"
        )
        file = db_session.find_one(cls.find(source, year=year, day=day))
        if not file:
            logger.debug("> Creating new File")
            file = cls(
                name=name,
                year=year,
                day=day,
                processed=processed,
                available_metadata=metadata,
            )
            if source:
                if isinstance(source, uuid.UUID):
                    file.source_id = source
                else:
                    file.source = source
            if add_session:
                db_session.add(file)
        return file
