# ruff: noqa: N802
from seedpsd.tools.common import is_temporary_network


class TestIsTemporaryNetwork:
    """
    Tests of the function "is_temporary_network()"
    """

    def test_permanent_FR(self):
        assert is_temporary_network("FR") is False

    def test_permanent_G(self):
        assert is_temporary_network("G") is False

    def test_temporary_Z3(self):
        assert is_temporary_network("Z3") is True

    def test_temporary_8C(self):
        assert is_temporary_network("8C") is True
